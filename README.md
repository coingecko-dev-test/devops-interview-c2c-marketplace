# DevOps Interview - C2C Marketplace

You are the new DevOps hire in **Memesy**, an online peer-to-peer marketplace startup for users to make, sell and buy the dankest memes on the internet.

## Requirements

On Memesy, users can list and purchase memes in image, video and audio formats. The Memesy MVP has the following requirements:

- Users will primarily use email and password-based authentication.
- Memesy is a peer-to-peer marketplace and they are expecting equal amounts of listing and browsing activities on the site
- Memesy is discovering their market and needs to adapt quickly to changes in data structure as the product grows.
- Buyers can purchase multiple assets in a single payment transaction. Once purchased, each buyer has a unique access link to their copy of the asset.

Memesy will launch in US markets, and the business is expecting 1M monthly active users within the first quarter.

You are preparing for a meeting with the developers this Friday, and you will be proposing a stack to deploy Memesy to production.

## Scoring guide

Explain your choice of tools and any relevant assumptions you made in the solution. We recommend implementing tools which you are most familiar with, and taking **scalability, extensibility and cost** into consideration.

**You can include any amount of detail in your solution**: READMEs, architecture diagrams, data modelling or free text.

While implementation details are helpful in our assessment, **we do not expect your solution to be exhaustive**. Do only spend the necessary amount of time on your solution.

A successful solution should be succinct, viable and illustrate the depth-breadth of your working knowledge. Your submission will be used as a basis of the next whiteboarding round.

You may use our [stackshare.io](https://stackshare.io/companies/coingecko) profile as a reference.
